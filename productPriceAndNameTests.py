import unittest
from selenium import webdriver


class ProductPriceAndNameTests (unittest.TestCase):

    @classmethod
    def setUp(self):
        self.base_url = 'https://autodemo.testoneo.com/en/'
        self.sample_product_url = self.base_url + 'men/1-1-hummingbird-printed-t-shirt.html'
        self.driver = webdriver.Chrome(executable_path=r"/home/marcin/Pobrane/chromedriver2.41/chromedriver")

    @classmethod
    def tearDown(self):
        self.driver.quit()

    def test_check_product_name(self):
        expected_product_name = 'HUMMINGBIRD PRINTED T-SHIRT'
        driver = self.driver
        driver.get(self.sample_product_url)
        name_element = driver.find_element_by_xpath('//h1[@class="h1"]')
        name_element_text = name_element.text
        self.assertEqual(expected_product_name, name_element_text,
                         f'Expected text differ from actual for page url: {self.sample_product_url}')

    def test_check_product_price(self):
        expected_product_price = 'PLN23.52'
        driver = self.driver
        driver.get(self.sample_product_url)
        price_element = driver.find_element_by_xpath('//span[contains(text(),"PLN23.52")]')
        price_element_text = price_element.text
        self.assertEqual(expected_product_price, price_element_text,
                         f'Expected text differ from actual for page url: {self.sample_product_url}')

if __name__ == "__main__":
    unittest.main()